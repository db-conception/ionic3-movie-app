import { Component } from "@angular/core";
import { IonicPage, NavController, NavParams } from "ionic-angular";
import { IMovie } from "../../model/IMovie";
import { MovieDetailPage } from "../movie-detail/movie-detail";
import { MovieApiProvider } from "../../providers/movie-api/movie-api";

@IonicPage()
@Component({
  selector: "page-movie-list",
  templateUrl: "movie-list.html"
})
export class MovieListPage {
  movies = new Array<IMovie>();
 
  constructor(public navCtrl: NavController, public navParams: NavParams,
    private movieApiProvider: MovieApiProvider) {}
 
  ionViewDidLoad() {
    console.log("ionViewDidLoad MovieListPage");
    this.movieApiProvider.getMovies().subscribe(data =>{
      this.movies = data as Array<IMovie>;
    });
  }
 
  goToDetail(movie: IMovie) {
    this.navCtrl.push(MovieDetailPage, movie);
  }
}
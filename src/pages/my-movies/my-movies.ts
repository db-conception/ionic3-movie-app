import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { MovieListPage } from '../movie-list/movie-list';
import { FavoriteMovieProvider } from '../../providers/favorite-movie/favorite-movie';
import { IMovie } from '../../model/IMovie';
import { MovieDetailPage } from '../movie-detail/movie-detail';

/**
 * Generated class for the MyMoviesPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-my-movies',
  templateUrl: 'my-movies.html',
})
export class MyMoviesPage {
  favoriteMovies: IMovie[] = [];

  constructor(public navCtrl: NavController,
     public navParams: NavParams,
     private favoriteMovieProvider: FavoriteMovieProvider) {
  }

  ionViewDidLoad() {
    console.log("ionViewDidLoad MyMoviesPage");
  }
 
  findMovie() {
    this.navCtrl.push(MovieListPage);
  }

  ionViewWillEnter(){
    console.log("ionViewWillEnter MyMoviesPage");
    this.initFavoriteMovies();
  }
  private initFavoriteMovies() {
    this.favoriteMovieProvider.getFavoriteMovies()
    .then(favs => (this.favoriteMovies = favs))
  }

  goToDetail(movie : IMovie){
    console.log('go to detail');
    this.navCtrl.push(MovieDetailPage, movie);
  }
}

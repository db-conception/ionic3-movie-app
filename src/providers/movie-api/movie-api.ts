import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { IMovie } from '../../model/IMovie';
//import { Platform } from 'ionic-angular/umd/platform/platform';
import { Observable } from 'rxjs/Rx';
import { Platform } from 'ionic-angular';

/*
  Generated class for the MovieApiProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class MovieApiProvider {
  /*
    constructor(public http: HttpClient) {
      console.log('Hello MovieApiProvider Provider');
    }
  */
  movies: IMovie[];
  private baseUrl: string = "../../assets/api/movies.json";

  constructor(
    private readonly http: HttpClient,
    public platform: Platform
    //private readonly platform: Platform
  ) {
    console.log("Hello MovieApiProvider Provider");
    //on remplace le path si on est sur android.
    if (this.platform.is("cordova") && this.platform.is("android")) {
      this.baseUrl = "/android_asset/www/assets/api/movies.json";
    }
  }

  getMovies(): Observable<Object> {
    return this.http.get(`${this.baseUrl}`);
  }
}
